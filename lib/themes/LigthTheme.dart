import 'package:flutter/material.dart';
import 'package:grocery_fruits/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: ColorHelper.whiteDarker,
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
  fontFamily: 'Arial'
);
