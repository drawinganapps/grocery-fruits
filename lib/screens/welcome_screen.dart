import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/helper/color_helper.dart';
import 'package:grocery_fruits/drawers/circle_container.dart';
import 'package:grocery_fruits/routes/AppRoutes.dart';
import 'package:grocery_fruits/widgets/default_loading_widget.dart';
import 'package:loader_overlay/loader_overlay.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        children: [
          ClipPath(
            clipper: CircleContainer(),
            child: Container(
              width: screenWidth,
              padding: EdgeInsets.only(top: screenHeight * 0.05),
              decoration: BoxDecoration(
                  color: ColorHelper.tertiary,
                  image: const DecorationImage(
                      image: AssetImage('assets/img/background.png'),
                      fit: BoxFit.cover)),
              child: Center(
                child: Image.asset('assets/img/delivery_man.png',
                    height: screenHeight * 0.53, fit: BoxFit.fitHeight),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 50),
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                Text('Buy Your Daily',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 35, fontWeight: FontWeight.w600)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Grocery',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                            color: ColorHelper.primary,
                            fontSize: 35,
                            fontWeight: FontWeight.w600)),
                    Container(
                      margin: const EdgeInsets.only(left: 5),
                    ),
                    Text('Easily',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                            fontSize: 35, fontWeight: FontWeight.w600)),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Text(
                      "The easiest way to share your family's grocery shopping-Try it out.",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey)),
                )
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: SizedBox(
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ElevatedButton(
                onPressed: () {
                  context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                  Future.delayed(const Duration(seconds: 3), () {
                    Get.toNamed(AppRoutes.HOME);
                    context.loaderOverlay.hide();
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: ColorHelper.primary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: SizedBox(
                  width: screenWidth * 0.5,
                  height: 50,
                  child: Center(
                    child: Text("Let's Buy",
                        style: GoogleFonts.poppins(
                            color: ColorHelper.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 16)),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
