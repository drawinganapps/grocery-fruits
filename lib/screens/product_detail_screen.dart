import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/controller/counter_controller.dart';
import 'package:grocery_fruits/controller/item_controller.dart';
import 'package:grocery_fruits/controller/item_description_controller.dart';
import 'package:grocery_fruits/data/Dummy.dart';
import 'package:grocery_fruits/drawers/circle_container.dart';
import 'package:grocery_fruits/helper/color_helper.dart';
import 'package:grocery_fruits/models/item_model.dart';
import 'package:grocery_fruits/widgets/default_loading_widget.dart';
import 'package:grocery_fruits/widgets/item_counter_widget.dart';
import 'package:grocery_fruits/widgets/product_detail_description.dart';
import 'package:grocery_fruits/widgets/related_item_widget.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ProductDetailScreen extends StatelessWidget {
  const ProductDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final ItemDescriptionController desController =
        Get.find<ItemDescriptionController>();
    final CounterController counterController = Get.find<CounterController>();
    return GetBuilder<ItemController>(builder: (controller) {
      final ItemModel item = controller.selectedItem;
      final List<ItemModel> relatedItems =
          Dummy.itemList.where((element) => element.id != item.id).toList();
      relatedItems.shuffle();

      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leadingWidth: 65,
          title: Text(item.itemName,
              style: GoogleFonts.poppins(
                  color: Colors.black, fontWeight: FontWeight.w500)),
          backgroundColor: ColorHelper.tertiary,
          elevation: 0,
          leading: Container(
            padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
            child: Container(
              decoration: BoxDecoration(
                  color: ColorHelper.secondary.withOpacity(0.15),
                  borderRadius: BorderRadius.circular(10)),
              child: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(Icons.arrow_back, color: ColorHelper.secondary),
              ),
            ),
          ),
        ),
        body: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            ClipPath(
              clipper: CircleContainer(),
              child: Container(
                width: screenWidth,
                height: screenHeight * 0.35,
                decoration: BoxDecoration(
                  color: ColorHelper.tertiary,
                ),
                padding: const EdgeInsets.only(
                    left: 15, right: 15, top: 20, bottom: 20),
                child: Image.asset('assets/img/${item.itemImage}'),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [ItemCounterWidget()],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: screenWidth * 0.05, right: screenWidth * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.itemName,
                          style: GoogleFonts.poppins(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 25)),
                      Container(
                        margin: const EdgeInsets.only(top: 5),
                      ),
                      Text('Available in stock',
                          style: GoogleFonts.poppins(
                              color: Colors.black.withOpacity(0.3),
                              fontWeight: FontWeight.w600,
                              fontSize: 15)),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          Text('\$${item.itemPrice.toString()}',
                              style: GoogleFonts.poppins(
                                  color: ColorHelper.primary,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 25)),
                          Text('/kg',
                              style: GoogleFonts.poppins(
                                  color: Colors.black.withOpacity(0.3),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16)),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5),
                      ),
                      Row(
                        children: [
                          const Icon(Icons.star_rate,
                              color: Colors.orangeAccent, size: 20),
                          Container(
                            margin: const EdgeInsets.only(left: 3),
                          ),
                          Text(item.itemRatings.toString(),
                              style: GoogleFonts.poppins(
                                  color: Colors.black.withOpacity(0.3),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16)),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20),
              padding: EdgeInsets.only(
                  left: screenWidth * 0.05, right: screenWidth * 0.05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Details',
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 18)),
                  Container(
                    margin: const EdgeInsets.only(top: 15),
                    child: const ProductDetailDescription(),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20, bottom: 20),
              padding: EdgeInsets.only(
                  left: screenWidth * 0.05, right: screenWidth * 0.05),
              child: Text('Related Fruits',
                  style: GoogleFonts.poppins(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 18)),
            ),
            SizedBox(
              height: 80,
              child: ListView(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                children: List.generate(relatedItems.length, (index) {
                  return GestureDetector(
                    onTap: () {
                      context.loaderOverlay
                          .show(widget: const DefaultLoadingWidget());
                      Future.delayed(const Duration(seconds: 1), () {
                        controller.changeItem(relatedItems.elementAt(index));
                        desController.reset();
                        context.loaderOverlay.hide();
                      });
                    },
                    child:
                        RelatedItemWidget(item: relatedItems.elementAt(index)),
                  );
                }),
              ),
            )
          ],
        ),
        bottomNavigationBar: SizedBox(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    context.loaderOverlay
                        .show(widget: const DefaultLoadingWidget());
                    Future.delayed(const Duration(seconds: 1), () {
                      context.loaderOverlay.hide();
                      final snackBar = SnackBar(
                        backgroundColor: ColorHelper.tertiary.withOpacity(0.9),
                        content: Text(
                            'Yey!! ${counterController.orderCounter} Kg of ${item.itemName} has been added to the cart!',
                            style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w500)),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      counterController.reset();
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    primary: ColorHelper.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  child: SizedBox(
                    width: screenWidth * 0.83,
                    height: 50,
                    child: Center(
                      child: Text("Add to cart",
                          style: GoogleFonts.poppins(
                              color: ColorHelper.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 16)),
                    ),
                  ),
                ),
              ],
            )),
      );
    });
  }
}
