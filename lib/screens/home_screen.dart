import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/controller/filter_controller.dart';
import 'package:grocery_fruits/data/Dummy.dart';
import 'package:grocery_fruits/helper/color_helper.dart';
import 'package:grocery_fruits/models/item_model.dart';
import 'package:grocery_fruits/models/items_filter.dart';
import 'package:grocery_fruits/widgets/filter_widget.dart';
import 'package:grocery_fruits/widgets/item_card_widget.dart';
import 'package:grocery_fruits/widgets/search_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<ItemsFilter> filterList = Dummy.filterList;
    List<ItemModel> items = Dummy.itemList;
    return GetBuilder<FilterController>(builder: (controller) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorHelper.whiteDarker,
            elevation: 0,
            leadingWidth: 65,
            title: Text('Home',
                style: GoogleFonts.poppins(
                    color: Colors.black, fontWeight: FontWeight.w500)),
            centerTitle: true,
            leading: Container(
              padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: ColorHelper.tertiary,
                    borderRadius: BorderRadius.circular(10)),
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.menu, color: ColorHelper.secondary),
                ),
              ),
            ),
            actions: [
              Container(
                width: 65,
                padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
                child: Container(
                    decoration: BoxDecoration(
                        color: ColorHelper.tertiary,
                        borderRadius: BorderRadius.circular(10)),
                    child: Badge(
                      badgeContent: Text('3',
                          style: GoogleFonts.poppins(
                            color: ColorHelper.white,
                          )),
                      badgeColor: ColorHelper.primary,
                      position: const BadgePosition(top: -1, end: 1),
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.shopping_bag_outlined,
                            color: ColorHelper.secondary),
                      ),
                    )),
              )
            ],
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 15, bottom: 10),
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Text("Let's find best food here",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500, fontSize: 22)),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                margin: const EdgeInsets.only(bottom: 15),
                child: const SearchWidget(),
              ),
              SizedBox(
                height: 35,
                child: ListView(
                    physics: const BouncingScrollPhysics(),
                    padding: const EdgeInsets.all(0),
                    scrollDirection: Axis.horizontal,
                    children: List.generate(filterList.length, (index) {
                      return GestureDetector(
                        onTap: () {
                          controller.changeFilter(index);
                        },
                        child: FilterCategoriesWidget(
                            isSelected: index == controller.selectedFilter,
                            filter: filterList[index]),
                      );
                    })),
              ),
              Container(
                margin: const EdgeInsets.only(top: 18),
              ),
              Expanded(
                child: GridView.count(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    crossAxisCount: 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    childAspectRatio: 0.7,
                    physics: const BouncingScrollPhysics(),
                    children: List.generate(items.length, (index) {
                      return ItemCardWidget(item: items[index]);
                    })),
              )
            ],
          ),
          bottomNavigationBar: Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            decoration: BoxDecoration(
              color: ColorHelper.white,
            ),
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      enableFeedback: false,
                      onPressed: () {},
                      icon: Icon(
                        Icons.home_rounded,
                        color: ColorHelper.primary,
                        size: 30,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          color: ColorHelper.primary, shape: BoxShape.circle),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.bookmark_border,
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      decoration: const BoxDecoration(
                          color: Colors.transparent, shape: BoxShape.circle),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      enableFeedback: false,
                      onPressed: () {},
                      icon: const Icon(
                        Icons.shopping_bag_outlined,
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      decoration: const BoxDecoration(
                          color: Colors.transparent, shape: BoxShape.circle),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      enableFeedback: false,
                      onPressed: () {},
                      icon: const Icon(
                        Icons.notifications_outlined,
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      decoration: const BoxDecoration(
                          color: Colors.transparent, shape: BoxShape.circle),
                    )
                  ],
                ),
              ],
            ),
          ));
    });
  }
}
