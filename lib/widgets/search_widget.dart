import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: TextField(
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.grey.withOpacity(0.08), width: 0.2)),
            filled: true,
            fillColor: Colors.grey.withOpacity(0.08),
            hintText: 'Search now...',
            prefixIcon:
            const Icon(Icons.search, color: Colors.black, size: 25),
            hintStyle: const TextStyle(
                fontWeight: FontWeight.w200, color: Colors.black)),
      ),
    );
  }
}
