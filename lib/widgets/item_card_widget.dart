import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_fruits/controller/item_controller.dart';
import 'package:grocery_fruits/helper/color_helper.dart';
import 'package:grocery_fruits/models/item_model.dart';
import 'package:grocery_fruits/routes/AppRoutes.dart';
import 'package:grocery_fruits/widgets/default_loading_widget.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;
  const ItemCardWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            color: ColorHelper.white,
            border: Border.all(color: ColorHelper.tertiary),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: item.isBookMarked
                          ? ColorHelper.secondary.withOpacity(0.3)
                          : ColorHelper.secondary.withOpacity(0.5)),
                  child: item.isBookMarked
                      ? Icon(Icons.bookmark,
                          size: 25, color: ColorHelper.primary)
                      : Icon(Icons.bookmark_border,
                          size: 25, color: ColorHelper.white),
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(seconds: 1), () {
                  context.loaderOverlay.hide();
                  Get.find<ItemController>().changeItem(item);
                  Get.toNamed(AppRoutes.PRODUCT_DETAIL);
                });
              },
              child: Column(
                children: [
                  Image.asset('assets/img/${item.itemImage}',
                      fit: BoxFit.fill, height: screenHeight * 0.12),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.itemName,
                          style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: Colors.black)),
                      Container(
                        margin: const EdgeInsets.only(top: 2),
                      ),
                      Text('Fresh Fruits',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              color: Colors.grey.withOpacity(0.8))),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('\$ ${item.itemPrice}',
                    style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: Colors.black)),
                Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: ColorHelper.primary),
                  child: Icon(Icons.add, size: 25, color: ColorHelper.white),
                )
              ],
            )
          ],
        ));
  }
}
