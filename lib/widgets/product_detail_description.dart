import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/controller/item_description_controller.dart';
import 'package:grocery_fruits/data/Dummy.dart';

class ProductDetailDescription extends StatelessWidget {
  const ProductDetailDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ItemDescriptionController>(builder: (controller) {
      return GestureDetector(
        onTap: () {
          controller.changeExpanded();
        },
        child: Text(Dummy.description,
            textAlign: TextAlign.justify,
            overflow: TextOverflow.fade,
            maxLines: controller.isExpanded ? 50 : 3,
            style: GoogleFonts.poppins(
                color: Colors.black.withOpacity(0.5),
                fontWeight: FontWeight.w500,
                fontSize: 15)),
      );
    });
  }
}
