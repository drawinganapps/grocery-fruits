import 'package:flutter/cupertino.dart';
import 'package:grocery_fruits/helper/color_helper.dart';
import 'package:grocery_fruits/models/item_model.dart';

class RelatedItemWidget extends StatelessWidget {
  final ItemModel item;
  const RelatedItemWidget({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: const EdgeInsets.all(5),
      margin: EdgeInsets.only(left: screenWidth * 0.05),
      decoration: BoxDecoration(
        color: ColorHelper.tertiary,
        borderRadius: BorderRadius.circular(15)
      ),
      child: Image.asset('assets/img/${item.itemImage}', width: 60),
    );
  }

}