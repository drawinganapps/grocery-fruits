import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/controller/counter_controller.dart';
import 'package:grocery_fruits/helper/color_helper.dart';

class ItemCounterWidget extends StatelessWidget {
  const ItemCounterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CounterController>(builder: (controller){
      return Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: ColorHelper.whiteDarker,
            borderRadius: BorderRadius.circular(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: const EdgeInsets.all(1),
                decoration: BoxDecoration(
                    color: ColorHelper.grey,
                    borderRadius: BorderRadius.circular(5)),
                margin: const EdgeInsets.only(right: 25),
                child: GestureDetector(
                    child: Icon(Icons.remove, size: 25, color: ColorHelper.white),
                    onTap: () {
                      controller.reduceOrder();
                    })),
            Row(
              children: [
                Text(controller.orderCounter.toString(),
                    style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 18)),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                ),
                Text('kg',
                    style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 16)),
              ],
            ),
            Container(
                padding: const EdgeInsets.all(1),
                decoration: BoxDecoration(
                    color: ColorHelper.primary,
                    borderRadius: BorderRadius.circular(5)),
                margin: const EdgeInsets.only(left: 25),
                child: GestureDetector(
                  onTap: () {
                    controller.addOrder();
                  },
                  child: Icon(Icons.add, size: 25, color: ColorHelper.white),
                )),
          ],
        ),
      );
    });
  }
}
