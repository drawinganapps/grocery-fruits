import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grocery_fruits/controller/filter_controller.dart';
import 'package:grocery_fruits/models/items_filter.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final ItemsFilter filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      return Center(
        child: Container(
          margin: const EdgeInsets.only(left: 15, right: 15),
          height: 40,
          child: Column(
            children: [
              Center(
                child: Text(filter.name,
                    style: GoogleFonts.poppins(
                        color: isSelected ? Colors.black : Colors.grey.withOpacity(0.5),
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                  color: isSelected ? Colors.black : Colors.transparent,
                  shape: BoxShape.circle
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
