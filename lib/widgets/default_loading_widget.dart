import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class DefaultLoadingWidget extends StatelessWidget {
  const DefaultLoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SpinKitPouringHourGlassRefined(
        color: Colors.red,
        size: 80.0,
        duration: Duration(milliseconds: 1000),
      ),
    );
  }
}
