import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(250, 250, 250, 1.0);
  static Color primary = const Color.fromRGBO(222,31,39, 1);
  static Color secondary = const Color.fromRGBO(203, 51, 62, 1.0);
  static Color tertiary = const Color.fromRGBO(251,228,229,1);
  static Color grey = const Color.fromRGBO(205,216,234, 1);
}