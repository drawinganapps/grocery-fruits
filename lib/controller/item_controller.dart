import 'package:get/get.dart';
import 'package:grocery_fruits/models/item_model.dart';

class ItemController extends GetxController {
  late ItemModel selectedItem;
  var isExpanded = false;
  var orderCounter = 1;

  void changeItem(ItemModel item) {
    selectedItem = item;
    isExpanded = false;
    update();
  }

  void changeExpanded(bool val)  {
    isExpanded = val;
    update();
  }
}
