import 'package:get/get.dart';

class ItemDescriptionController extends GetxController {
  var isExpanded = false;

  changeExpanded(){
    isExpanded = !isExpanded;
    update();
  }

  reset() {
    isExpanded = false;
  }
}