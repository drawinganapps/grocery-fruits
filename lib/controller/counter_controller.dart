import 'package:get/get.dart';
import 'package:grocery_fruits/models/item_model.dart';

class CounterController extends GetxController {
  var orderCounter = 1;

  void addOrder() {
    orderCounter += 1;
    update();
  }

  void reduceOrder() {
    if (orderCounter > 1) {
      orderCounter -= 1;
      update();
    }
  }

  void reset() {
    var orderCounter = 1;
    update();
  }
}
