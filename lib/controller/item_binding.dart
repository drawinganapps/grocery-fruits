import 'package:get/get.dart';
import 'package:grocery_fruits/controller/counter_controller.dart';
import 'package:grocery_fruits/controller/item_controller.dart';
import 'package:grocery_fruits/controller/item_description_controller.dart';

class ItemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemController>(() => ItemController());
    Get.lazyPut<CounterController>(() => CounterController());
    Get.lazyPut<ItemDescriptionController>(() => ItemDescriptionController());
  }
}
