import 'package:get/get.dart';
import 'package:grocery_fruits/controller/item_description_controller.dart';

class ItemDescriptionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemDescriptionController>(() => ItemDescriptionController());
  }
}
