class ItemModel {
  int id;
  String itemImage;
  String itemName;
  double itemPrice;
  double itemRatings;
  bool isBookMarked;

  ItemModel(this.id, this.itemImage, this.itemName, this.itemPrice, this.itemRatings, this.isBookMarked);
}
