import 'package:grocery_fruits/models/item_model.dart';
import 'package:grocery_fruits/models/items_filter.dart';

class Dummy {
  static List<ItemsFilter> filterList = <ItemsFilter>[
    const ItemsFilter('Fruits', 'Fruits'),
    const ItemsFilter('Vegetable', 'Vegetable'),
    const ItemsFilter('Bakery', 'Bakery'),
    const ItemsFilter('Organic Drinks', 'Drinks'),
    const ItemsFilter('Fast Food', 'Fast Food'),
  ];

  static List<ItemModel> itemList = [
    ItemModel(1, 'strawberry.png', 'Strawberry', 2.4, 5.0, true),
    ItemModel(2, 'mangga.png', 'Mango', 3.0, 4.9, false),
    ItemModel(3, 'banana.png', 'Banana', 1.4, 4.2, false),
    ItemModel(4, 'pomegranate.png', 'Pomegranate', 6.4, 4.7, true),
    ItemModel(5, 'jambu.png', 'Guava', 12.4, 4.7, true),
    ItemModel(6, 'kurma.png', 'Dates', 11.4, 4.7, true),
    ItemModel(7, 'durian.png', 'Durian', 7.4, 5.0, false),
    ItemModel(8, 'manggis.png', 'Mangosteen', 4.4, 5.0, false),
    ItemModel(9, 'nangka.png', 'Jackfruit', 2.4, 4.8, false),
    ItemModel(10, 'orange.png', 'Grapefruit', 1.4, 4.2, false),
    ItemModel(10, 'apel.png', 'Apple', 5.6, 4.2, false),
    ItemModel(10, 'pepaya.png', 'Pawpaw', 2.6, 4.2, false),
  ];

  static String description =
      "Kami adalah perusahaan manufaktur makanan yang mengembangkan buah untuk pasar terbaik, lingkungan terbaik, dan produk terbaik. Kami adalah pembuat buah dan kami bangga untuk mengatakan bahwa perusahaan produksi kami adalah Indonesia. Kami ingin membawa pengetahuan dan pengalaman kami kepada konsumen dan industri restoran. Kami ingin menciptakan rasa makanan yang lebih sehat dan menyenangkan dan kami ingin memberikan hasil terbaik kepada petani dan pelanggan kami.";
}
