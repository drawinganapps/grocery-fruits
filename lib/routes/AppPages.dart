import 'package:get/get.dart';
import 'package:grocery_fruits/controller/counter_binding.dart';
import 'package:grocery_fruits/controller/filter_binding.dart';
import 'package:grocery_fruits/controller/item_binding.dart';
import 'package:grocery_fruits/controller/item_description_binding.dart';
import 'package:grocery_fruits/screens/home_screen.dart';
import 'package:grocery_fruits/screens/product_detail_screen.dart';
import 'package:grocery_fruits/screens/welcome_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.WELCOME,
        page: () => const WelcomeScreen(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.HOME,
        page: () => const HomeScreen(),
        bindings: [FilterBinding(), ItemBinding()],
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.PRODUCT_DETAIL,
        page: () => const ProductDetailScreen(),
        binding: ItemBinding(),
        transition: Transition.rightToLeft),
  ];
}
