class AppRoutes {
  static const String WELCOME = '/';
  static const String HOME = '/home';
  static const String PRODUCT_DETAIL = '/product-detail';
}
